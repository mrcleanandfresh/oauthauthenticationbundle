<?php
/**
 * OAuthAuthenticationBundle.php
 *
 * Author: Kevin Dench <kevin.dench@intellidata.net>
 * Author: IntelliData, Inc.
 * License: Proprietary
 * Version: 1.0
 * Copyright: 2016.
 */

namespace OAuth\AuthenticationBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class OAuthAuthenticationBundle extends Bundle
{
}
