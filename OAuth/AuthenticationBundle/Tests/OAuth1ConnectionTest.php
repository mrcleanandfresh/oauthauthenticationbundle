<?php
/**
 * OAuth1ConnectionTest.php
 *
 * Author: Kevin Dench <kevin.dench@intellidata.net>
 * Author: IntelliData, Inc.
 * License: Proprietary
 * Version: 1.0
 * Copyright: 2016.
 */

namespace Tests\OAuth\AuthenticationBundle;

use OAuth\AuthenticationBundle\OAuth1Connection;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class OAuth1ConnectionTest extends WebTestCase
{
	/**
	 * @var $OAuth1Connection OAuth1Connection
	 */
	private $OAuth1Connection;

	public function setUp() {
		$consumer_key = 'VXD22AD9kcNyNgsfW6cwkWRkw';
		$consumer_secret = 'y0k3z9Y46V0DMAKGe4Az2aDtqNt9aXjg3ssCMCldUheGBT0YL9';
		$token = '3232926711-kvMvNK5mFJlUFzCdtw3ryuwZfhIbLJtPX9e8E3Y';
		$token_secret = 'EYrFp0lfNajBslYV3WgAGmpHqYZvvNxP5uxxSq8Dbs1wa';

		$this->OAuth1Connection = new OAuth1Connection($consumer_key, $consumer_secret, $token, $token_secret);
	}

	public function testQueryData()
	{
		return array(
			'post'		=> true,
			'status'	=> "publish",
			'title'		=> "Best Day Ever",
			'content'	=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque blandit neque orci, vel volutpat sem tincidunt vitae. Sed in neque sit amet ex auctor vestibulum et at sapien.",
			'limitTo'	=> 5
		);
	}

	public function testPostData()
	{
		return array(
			'post'		=> true,
			'status'	=> "publish",
			'title'		=> "Best Day Ever",
			'content'	=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque blandit neque orci, vel volutpat sem tincidunt vitae. Sed in neque sit amet ex auctor vestibulum et at sapien.",
			'limitTo'	=> 5
		);
	}

	public function testSetGETParameters()
	{
		$query = $this->testQueryData();
		$queryParms = $this->OAuth1Connection->setGETParameters($query);
		$expected = "?post=1&status=publish&title=Best+Day+Ever&content=Lorem+ipsum+dolor+sit+amet%2C+consectetur+adipiscing+elit.+Quisque+blandit+neque+orci%2C+vel+volutpat+sem+tincidunt+vitae.+Sed+in+neque+sit+amet+ex+auctor+vestibulum+et+at+sapien.&limitTo=5";

		$this->assertContains($expected, $queryParms);
	}

	public function testSetPOSTParameters()
	{
		$request = $this->testPostData();

		$data = $this->OAuth1Connection->setPOSTParameters($request);
		$expected = array(
			"post" => 'true',
			"status" => "publish",
			"title" => "Best Day Ever",
			"content" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque blandit neque orci, vel volutpat sem tincidunt vitae. Sed in neque sit amet ex auctor vestibulum et at sapien.",
			"limitTo" => 5,
		);

		$this->assertEquals($expected, $data);
	}

	public function testRequestResource()
	{
		$url    = 'https://api.twitter.com/1.1/statuses/mentions_timeline.json';
		$http_method = 'GET';
		$data = array(
			'max_id'	=> 595150043381915648
		);

		$request = $this->OAuth1Connection->requestResource($url, $http_method, $data);

		$connection = $this->OAuth1Connection;

		$this->assertTrue($connection::isValidJson($request));
	}
}