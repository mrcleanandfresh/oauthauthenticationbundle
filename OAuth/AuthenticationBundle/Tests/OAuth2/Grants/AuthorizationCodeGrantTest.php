<?php
/**
 * AuthorizationCodeGrantTest.php
 *
 * Author: Kevin Dench <kevin.dench@intellidata.net>
 * Author: IntelliData, Inc.
 * License: Proprietary
 * Version: 1.0
 * Copyright: 2016.
 */

/**
 * Created by PhpStorm.
 * User: Kevin Dench
 * Date: 9/26/2016
 * Time: 7:31 AM
 */

namespace OAuth\AuthenticationBundle\Tests\OAuth2\Grants;


use OAuth\AuthenticationBundle\Version\Excep\OAuth2Exception;
use OAuth\AuthenticationBundle\Version\OAuth2\Grants\AuthorizationCodeGrant;
use OAuth\AuthenticationBundle\Version\OAuth2\OAuth2Config;
use OAuth\AuthenticationBundle\Version\OAuth2\OAuth2Validator;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\DependencyInjection\Container;

class AuthorizationCodeGrantTest extends WebTestCase
{
	/**
	 * @var $AuthorizationCodeGrant AuthorizationCodeGrant
	 */
	private $AuthorizationCodeGrant;

	public function setUp()
	{
		$client_id = "450013749819-uhhl7nhm1prh8b229fhkogqchoqdvepc.apps.googleusercontent.com";
		$client_secret = "JDcPv0jwlc6XsJAf24O0tSxh";
		
		$Config = new OAuth2Config(
			$client_id,
			$client_secret,
			'http://localhost:8888/app_dev.php/oauth/redirect',
			'code',
			'https://accounts.google.com/o/oauth2/v2/auth',
			'https://www.googleapis.com/oauth2/v4/token'
		);

		$Validator = new OAuth2Validator();

		$this->AuthorizationCodeGrant = new AuthorizationCodeGrant($Config, $Validator);
	}

	/**
	 * Once we get the access token we need to handle the response based on the specific flow we're in
	 *
	 * @return object|void
	 * @throws OAuth2Exception
	 */
	public function testHandleAccessTokenResponse()
	{

	}

	/**
	 * Exchange authentication information for tokens, depending on grant_type
	 *
	 * @return $this
	 * @throws OAuth2Exception
	 */
	public function testAuthenticate()
	{

	}

	/**
	 * This flow has specific grant parameters
	 *
	 * @return array
	 * @throws OAuth2Exception
	 */
	public function testSetGrantParameters()
	{

	}

	/**
	 * Build the Authorization URL
	 */
	public function testBuildOAuthURL()
	{
		$client = static::createClient();

		$crawler = $client->request('GET', '/home');

		$link = $crawler->selectLink('Connect to whoever')->link()->getUri();

		$grants = array(
			'https://www.googleapis.com/auth/plus.login',
			'https://www.googleapis.com/auth/plus.me',
			'https://www.googleapis.com/auth/userinfo.email',
			'https://www.googleapis.com/auth/userinfo.profile',
		);

		$expectedLink = $this->AuthorizationCodeGrant->buildOAuthURL($grants);

		$this->assertSame($expectedLink, $link);
	}

	/**
	 * Build the Authorization URL
	 */
	public function testTheBuiltOAuthURL()
	{
		$grants = array(
			'https://www.googleapis.com/auth/plus.login',
			'https://www.googleapis.com/auth/plus.me',
			'https://www.googleapis.com/auth/userinfo.email',
			'https://www.googleapis.com/auth/userinfo.profile',
		);

		$curlUrl = $this->AuthorizationCodeGrant->buildOAuthURL($grants);
	}

}