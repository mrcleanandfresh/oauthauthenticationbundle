<?php
/**
 * ResourceOwnerPasswordTest.php
 *
 * Author: Kevin Dench <kevin.dench@intellidata.net>
 * Author: IntelliData, Inc.
 * License: Proprietary
 * Version: 1.0
 * Copyright: 2016.
 */

/**
 * Created by PhpStorm.
 * User: Kevin Dench
 * Date: 9/23/2016
 * Time: 2:47 PM
 */

namespace OAuth\AuthenticationBundle\Tests\OAuth2\Grants;


use OAuth\AuthenticationBundle\Version\OAuth2\Grants\ResourceOwnerPassword;
use OAuth\AuthenticationBundle\Version\OAuth2\OAuth2Config;
use OAuth\AuthenticationBundle\Version\OAuth2\OAuth2Validator;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ResourceOwnerPasswordTest extends WebTestCase{

	private $OAuthResourceOwnerPassword;

	public function setUp()
	{
		$client_id = '3c1f5ba462006ddbfe4bc8a11898dabd';
		$client_secret = 'd95147dcc0818d676675f07a564c749e7e353a428069ba07';
		$username = "Intelli_SleuthTesting_data16";
		$password = "uproar mater whiff surge muffin";

		$redirect_uri = null;
		$response_type = 'code';
		$authorization_url = null;
		$token_url = "http://localhost:8080/component-edge/oauth/token";

		$OAuthConfig = new OAuth2Config($client_id, $client_secret, $redirect_uri, $response_type, $authorization_url, $token_url);

		$this->OAuthResourceOwnerPassword = new ResourceOwnerPassword($OAuthConfig, new OAuth2Validator(), $username, $password);
	}

	public function testAuthenticateAndHandleTokenResponse()
	{
		$this->OAuthResourceOwnerPassword->authenticate();

		$payload = $this->OAuthResourceOwnerPassword->getRefreshToken();

		$this->assertNotNull($payload);
	}

	public function testGetGrantParameters()
	{
		$scopes = array(
			'https://www.googleapis.com/auth/plus.me',
			'https://www.googleapis.com/auth/plus.login',
			'https://www.googleapis.com/auth/userinfo.profile'
		);

		$resultingArray = $this->OAuthResourceOwnerPassword->getGrantParameters($scopes);

		$this->assertArrayHasKey('scope', $resultingArray);
	}
}