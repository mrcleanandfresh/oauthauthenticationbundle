<?php
/**
 * OAuth2Exception.php
 *
 * Author: Kevin Dench <kevin.dench@intellidata.net>
 * Author: IntelliData, Inc.
 * License: Proprietary
 * Version: 1.0
 * Copyright: 2016.
 */

/**
 * Created by PhpStorm.
 * User: Kevin Dench
 * Date: 5/31/2016
 * Time: 9:30 AM
 */

namespace OAuth\AuthenticationBundle\Version\Excep;


class OAuth2Exception extends \Exception {

}