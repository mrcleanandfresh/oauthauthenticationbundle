<?php
/**
 * AuthorizationCodeGrant.php
 *
 * Author: Kevin Dench <kevin.dench@intellidata.net>
 * Author: IntelliData, Inc.
 * License: Proprietary
 * Version: 1.0
 * Copyright: 2016.
 */

namespace OAuth\AuthenticationBundle\Version\OAuth2\Grants;

use OAuth\AuthenticationBundle\Version\Excep\OAuth2Exception;
use OAuth\AuthenticationBundle\Version\HTTPRequestService;
use OAuth\AuthenticationBundle\Version\OAuth2\Impl\RedirectGrant;
use OAuth\AuthenticationBundle\Version\OAuth2\OAuth2Config;
use OAuth\AuthenticationBundle\Version\OAuth2\OAuth2Flow;
use OAuth\AuthenticationBundle\Version\OAuth2\OAuth2Validator;

/**
 * Class AuthorizationCodeGrant
 * @package AppBundle\Service\Authentication\OAuth\OAuth2\Grants
 */
class AuthorizationCodeGrant extends OAuth2Flow implements RedirectGrant
{
    /**
     * Authorization code grant value
     * @const CODE_GRANT
     */
    const CODE_GRANT 	 = "authorization_code";

	/**
	 * The type of token that has been given, typically "Bearer"
	 * @var $token_type
	 */
	protected $token_type;

	/**
	 * The code received to exchange for tokens
	 * @var $code
	 */
	protected $code;

	/**
	 * Instance of the OAuth2Validator
	 * @var OAuth2Validator $validator
	 */
	private $validator;

	/**
	 * AuthorizationCodeGrant constructor.
	 *
	 * @param OAuth2Config    $config
	 * @param OAuth2Validator $validator
	 */
	public function __construct(OAuth2Config $config, OAuth2Validator $validator)
	{
		$this->setConfig($config);
		$this->validator = $validator;
	}

	/**
	 * Once we get the access token we need to handle the response based on the specific flow we're in
	 * 
	 * @param $response
	 *
	 * @return object|void
	 * @throws OAuth2Exception
	 */
	public function handleAccessTokenResponse($response) {
		$response_obj = json_decode($response);
		if (property_exists($response_obj, 'state')) {
			if(OAuth2Validator::checkState($this->getState(), $response_obj->state)) {
				if (property_exists($response_obj, 'code')) {
					$this->setAuthorizationCode($response_obj->code);
				}
			} else {
				throw new OAuth2Exception("States do not match!");
			}
		} else {
			if (property_exists($response_obj, 'code')) {
				$this->setAuthorizationCode($response_obj->code);
			}
		}

		return $this;
	}

	/**
	 * Exchange authentication information for tokens, depending on grant_type
	 *
	 * @return $this
	 * @throws OAuth2Exception
	 */
	public function authenticate() {
        $config = $this->getConfig();
        $url = $config->getAuthorizationUrl();
		
		$optional_param = array(
			'client_secret'	=> $this->getConfig()->getClientSecret(),
			'scope'			=> $this->getScope()
		);

		$parameters = array(
			'grant_type'	=> $this::CODE_GRANT,
			'code'			=> $this->getCode(),
			'redirect_uri'	=> $this->getConfig()->getRedirectUri(),
			'client_id'		=> $this->getConfig()->getClientId(),
		) + $optional_param;

		$addl_options = array(
			CURLOPT_POST			=> true,
			CURLOPT_POSTFIELDS		=> http_build_query($parameters)
		);

		$payload = HTTPRequestService::performRequest($url, $config::IS_SSL_ENABLED, $addl_options);

		$this->handleAccessTokenResponse($payload);

		return $this;
	}

	/**
	 * This flow has specific grant parameters
	 * 
	 * @param $scopes
	 * @return array
	 * @throws OAuth2Exception
	 */
	public function getGrantParameters(array $scopes) {
		if (!empty($scopes)) {
			foreach ($scopes as $scope) {
				$this->addScope($scope);
			}
		}

		// using the "application/x-www-form-urlencoded" format
		$this->setState(time());
		$optional_params = array(
			'redirect_uri'	=> $this->config->getRedirectUri(),
			'scope'			=> $this->getScope(),
			'access_type'	=> 'offline'
		);
		$send_tokens = array(
			'response_type'	=> 'code',
			'client_id'		=> $this->config->getClientId(),
			'state'			=> $this->getState()
		) + $optional_params;

		return $send_tokens;
	}

	/**
	 * Build the Authorization URL
	 * @example http://localhost:8080/component-edge/oauth
	 *                ?client_id=3c1f5ba462006ddbfe4bc8a11898dabd
	 *                &state=1464295395
	 *                &redirect_uri=https%3A%2F%2Foauth.intellidata.net%2Foauthcallback.php
	 *
	 * @param $scope
	 *
	 * @return string
	 */
	public function buildOAuthURL($scope) {
		$config = $this->config;
		return $config->getAuthorizationUrl()."?".http_build_query($this->getGrantParameters($scope));
	}
	
	/*
	 * Getters and Setters
	 * These are Generic Getters and setters
	 *************************************************/
	/**
	 * @return mixed
	 */
	public function getTokenType() {
		return $this->token_type;
	}

	/**
	 * @param mixed $token_type
	 *
	 * @return $this
	 */
	public function setTokenType($token_type) {
		$this->token_type = $token_type;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getCode() {
		return $this->code;
	}

	/**
	 * @param mixed $code
	 *
	 * @return $this
	 */
	public function setCode($code) {
		$this->code = $code;

		return $this;
	}

	/**
	 * @return OAuth2Config
	 */
	public function getConfig() {
		return $this->config;
	}

	/**
	 * @param OAuth2Config $config
	 */
	public function setConfig($config) {
		$this->config = $config;
	}
}