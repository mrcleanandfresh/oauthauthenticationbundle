<?php
/**
 * ClientCredentialsGrant.php
 *
 * Author: Kevin Dench <kevin.dench@intellidata.net>
 * Author: IntelliData, Inc.
 * License: Proprietary
 * Version: 1.0
 * Copyright: 2016.
 */

namespace OAuth\AuthenticationBundle\Version\OAuth2\Grants;

use OAuth\AuthenticationBundle\Version\HTTPRequestService;
use OAuth\AuthenticationBundle\Version\OAuth2\OAuth2Config;
use OAuth\AuthenticationBundle\Version\OAuth2\OAuth2Flow;
use OAuth\AuthenticationBundle\Version\OAuth2\OAuth2Validator;
use OAuth\AuthenticationBundle\Version\Excep\OAuth2Exception;

/**
 * Class ClientCredentialsGrant
 * @package AppBundle\Service\Authentication\OAuth\OAuth2\Grants
 */
class ClientCredentialsGrant extends OAuth2Flow
{
    /**
     * Client Credentials Grant
     * @const CLIENT_GRANT
     */
    const CLIENT_GRANT 	 = "client_credentials";

	/**
	 * Instance of the OAuth2Validator
	 * @var OAuth2Validator $validator
	 */
	private $validator;

	/**
	 * Instance of the OAuth2Config
	 * @var OAuth2Config $config
	 */
	protected $config;

	/**
	 * ClientCredentialsGrant constructor.
	 *
	 * @param OAuth2Config    $config
	 * @param OAuth2Validator $validator
	 */
	public function __construct(OAuth2Config $config, OAuth2Validator $validator)
	{
		$this->setConfig($config);
		$this->validator = $validator;
	}

	/**
	 * Once we get the access token we need to handle the response based on the specific flow we're in
	 *
	 * @param $response
	 *
	 * @return object|void
	 */
	public function handleAccessTokenResponse($response) {
		$response_obj = json_decode($response);
		if(property_exists($response_obj, 'access_token')) {
			(property_exists($response_obj, 'access_token')) ? $this->setAccessToken($response_obj->access_token) : $this->setAccessToken(null);
			(property_exists($response_obj, 'token_type') ? $this->setTokenType($response_obj->token_type) : $this->setTokenType(null));
			(property_exists($response_obj, 'expires_in')) ? $this->setTimeToExpire($response_obj->expires_in + time()) : $this->setTimeToExpire(null);
		}

		return $this;
	}

	/**
	 * Exchange authentication information for tokens, depending on grant_type
	 *
	 * @return $this
	 * @throws OAuth2Exception
	 */
	public function authenticate() {
        $url = $this->getConfig()->getAuthorizationUrl();
        $config = $this->getConfig();
		
		$parameters = array(
			'scope'			=> $this->getScope(),
			'grant_type'	=> $this::CLIENT_GRANT
		);

		$addl_options = array(
			CURLOPT_POST			=> true,
			CURLOPT_POSTFIELDS		=> http_build_query($parameters)
		);

		$payload = HTTPRequestService::performRequest($url, $config::IS_SSL_ENABLED, $addl_options);

		$this->handleAccessTokenResponse($payload);

		return $this;
	}

	/**
	 * This flow has specific grant parameters
	 *
	 * @param $scopes
	 * @return array
	 * @throws OAuth2Exception
	 */
	public function getGrantParameters(array $scopes) {
		if (!empty($scopes)) {
			foreach ($scopes as $scope) {
				$this->addScope($scope);
			}
		}

		$optional_params = array(
			'scope'			=> $this->getScope(),
			'access_type'	=> 'offline'
		);
		$send_tokens = array(
			'grant_type'	=> 'client_credentials'
		) + $optional_params;

		return $send_tokens;
	}

	// Possibly need to implement the BuildOAuthURL method here, but we have no use for it right now.
	
	/**
	 * @return OAuth2Config
	 */
	public function getConfig() {
		return $this->config;
	}

	/**
	 * @param OAuth2Config $config
	 */
	public function setConfig($config) {
		$this->config = $config;
	}
}