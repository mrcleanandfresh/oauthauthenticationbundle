<?php
/**
 * ImplicitGrant.php
 *
 * Author: Kevin Dench <kevin.dench@intellidata.net>
 * Author: IntelliData, Inc.
 * License: Proprietary
 * Version: 1.0
 * Copyright: 2016.
 */

namespace OAuth\AuthenticationBundle\Version\OAuth2\Grants;

use OAuth\AuthenticationBundle\Version\HTTPRequestService;
use OAuth\AuthenticationBundle\Version\OAuth2\Impl\RedirectGrant;
use OAuth\AuthenticationBundle\Version\OAuth2\OAuth2Config;
use OAuth\AuthenticationBundle\Version\OAuth2\OAuth2Flow;
use OAuth\AuthenticationBundle\Version\OAuth2\OAuth2Validator;
use OAuth\AuthenticationBundle\Version\Excep\OAuth2Exception;

/**
 * Class ImplicitGrant
 * @package AppBundle\Service\Authentication\OAuth\OAuth2\Grants
 */
class ImplicitGrant extends OAuth2Flow implements RedirectGrant
{
    /**
     * Implicit Grant
     * @const IMPLICIT GRANT
     */
    const IMPLICIT_GRANT = "authorization_code";

	/**
	 * Usually "Bearer" but could be something else
	 * @var $token_type
	 */
	protected $token_type;

	/**
	 * The authorization code.
	 * @var $code
	 */
	protected $code;
	
	/**
	 * Instance of the OAuth2Validator
	 * @var OAuth2Validator $validator
	 */
	private $validator;

	/**
	 * ImplicitGrant constructor.
	 *
	 * @param OAuth2Config    $config
	 * @param OAuth2Validator $validator
	 */
	public function __construct(OAuth2Config $config, OAuth2Validator $validator)
	{
		$this->setConfig($config);
		$this->validator = $validator;
	}

	/**
	 * Once we get the access token we need to handle the response based on the specific flow we're in
	 *
	 * @param $response
	 *
	 * @return object|void
	 * @throws OAuth2Exception
	 */
	public function handleAccessTokenResponse($response) {
		$response_obj = json_decode($response);
		if (property_exists($response_obj, 'state')) {
			try {
				if(OAuth2Validator::checkState($this->getState(), $response_obj->state)) {
					(property_exists($response_obj, 'access_token')) ? $this->setAccessToken($response_obj->access_token) : $this->setAccessToken(null);
					(property_exists($response_obj, 'token_type') ? $this->setTokenType($response_obj->token_type) : $this->setTokenType(null));
					(property_exists($response_obj, 'expires_in')) ? $this->setTimeToExpire($response_obj->expires_in + time()) : $this->setTimeToExpire(null);
					// If the scopes are different from the original request, reset the scopes
					if(property_exists($response_obj, 'scope')) {
						if(!OAuth2Validator::checkScopes($this->getState(), $response_obj->scopes)) {
							$this->setScope($response_obj->scopes);
						}
					}
				}
			} catch (OAuth2Exception $e) {
				return $e->getMessage();
			}
		} else {
			(property_exists($response_obj, 'access_token')) ? $this->setAccessToken($response_obj->access_token) : $this->setAccessToken(null);
			(property_exists($response_obj, 'token_type') ? $this->setTokenType($response_obj->token_type) : $this->setTokenType(null));
			(property_exists($response_obj, 'expires_in')) ? $this->setTimeToExpire($response_obj->expires_in) : $this->setTimeToExpire(null);
			// If the scopes are different from the original request, reset the scopes
			if(property_exists($response_obj, 'scope')) {
				if(!OAuth2Validator::checkScopes($this->getState(), $response_obj->scopes)) {
					$this->setScope($response_obj->scopes);
				}
			}
		}

		return $this;
	}

	/**
	 * Exchange authentication information for tokens, depending on grant_type
	 *
	 * @return $this
	 * @throws OAuth2Exception
	 */
	public function authenticate() {
			$config = $this->getConfig();
			$url = $config->getAuthorizationUrl();

		$parameters = array(
			'code'			=> $this->getCode(),
			'redirect_uri'	=> $this->getConfig()->getRedirectUri(),
			'client_id'		=> $this->getConfig()->getClientId(),
			'client_secret'	=> $this->getConfig()->getClientSecret(),
			'scope'			=> $this->scope,
			'grant_type'	=> $this::IMPLICIT_GRANT
		);

		$addl_options = array(
			CURLOPT_POST			=> true,
			CURLOPT_POSTFIELDS		=> http_build_query($parameters)
		);

		$payload = HTTPRequestService::performRequest($url, $config::IS_SSL_ENABLED, $addl_options);

		$this->handleAccessTokenResponse($payload);

		return $this;
	}

	/**
	 * This flow has specific grant parameters
	 *
	 * @param $scopes
	 * @return array
	 * @throws OAuth2Exception
	 */
	public function getGrantParameters(array $scopes) {
		if (!empty($scopes)) {
			foreach ($scopes as $scope) {
				$this->addScope($scope);
			}
		}

		$this->state = time();
		$optional_params = array(
			'redirect_uri'	=> $this->config->getRedirectUri(),
			'scope'			=> $this->getScope(),
			'access_type'	=> 'offline'
		);

		$send_tokens = array(
			'response_type'	=> 'token',
			'client_id'		=> $this->config->getClientId(),
			'state'			=> $this->getState()
		) + $optional_params;

		return $send_tokens;
	}

	/**
	 * Build the Authorization URL
	 * @example http://localhost:8080/component-edge/oauth
	 *                ?client_id=3c1f5ba462006ddbfe4bc8a11898dabd
	 *                &state=1464295395
	 *                &redirect_uri=https%3A%2F%2Foauth.intellidata.net%2Foauthcallback.php
	 *
	 * @param $scope
	 *
	 * @return string
	 */
	public function buildOAuthURL($scope) {
		$config = $this->config;
		return $config->getAuthorizationUrl()."?".http_build_query($this->getGrantParameters($scope));
	}

	/*
	 * Getters and Setters
	 * These are Generic Getters and setters
	 *************************************************/
	/**
	 * @return mixed
	 */
	public function getTokenType() {
		return $this->token_type;
	}

	/**
	 * @param mixed $token_type
	 *
	 * @return $this
	 */
	public function setTokenType($token_type) {
		$this->token_type = $token_type;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getCode() {
		return $this->code;
	}

	/**
	 * @param mixed $code
	 *
	 * @return $this
	 */
	public function setCode($code) {
		$this->code = $code;

		return $this;
	}

	/**
	 * @return OAuth2Config
	 */
	public function getConfig() {
		return $this->config;
	}

	/**
	 * @param OAuth2Config $config
	 */
	public function setConfig($config) {
		$this->config = $config;
	}
}