<?php
/**
 * ResourceOwnerPassword.php
 *
 * Author: Kevin Dench <kevin.dench@intellidata.net>
 * Author: IntelliData, Inc.
 * License: Proprietary
 * Version: 1.0
 * Copyright: 2016.
 */

namespace OAuth\AuthenticationBundle\Version\OAuth2\Grants;

use OAuth\AuthenticationBundle\Version\HTTPRequestService;
use OAuth\AuthenticationBundle\Version\OAuth2\OAuth2Config;
use OAuth\AuthenticationBundle\Version\OAuth2\OAuth2Flow;
use OAuth\AuthenticationBundle\Version\OAuth2\OAuth2Validator;
use OAuth\AuthenticationBundle\Version\Excep\OAuth2Exception;

/**
 * Class ResourceOwnerPassword
 * @package AppBundle\Service\Authentication\OAuth\OAuth2\Grants
 */
class ResourceOwnerPassword extends OAuth2Flow
{
    /**
     * Password Grant
     * @const PASSWORD_GRANT
     */
    const PASSWORD_GRANT = "password";

	/**
	 * @var
	 */
	private $username;
	/**
	 * @var
	 */
	private $password;

	/**
	 * Instance of the OAuth2Validator
	 * @var OAuth2Validator $validator
	 */
	private $validator;

	/**
	 * ResourceOwnerPassword constructor.
	 *
	 * @param OAuth2Config    $config
	 * @param OAuth2Validator $validator
	 * @param                 $username
	 * @param                 $password
	 */
	public function __construct(OAuth2Config $config, OAuth2Validator $validator, $username, $password)
	{
		$this->setUsername($username);
		$this->setPassword($password);

		$this->setConfig($config);
		$this->validator = $validator;
	}

	/**
	 * Once we get the access token we need to handle the response based on the specific flow we're in
	 *
	 * @param $response
	 *
	 * @return object|void
	 */
	public function handleAccessTokenResponse($response) {
		$response_obj = json_decode($response);
		if(property_exists($response_obj, 'error')) {
			$this->handleErrorResponse($response_obj);
		} elseif (property_exists($response_obj, 'access_token')) {
			(property_exists($response_obj, 'access_token')) 
				? $this->setAccessToken($response_obj->access_token)
				: $this->setAccessToken(null);
			(property_exists($response_obj, 'token_type') 
				? $this->setTokenType($response_obj->token_type)
				: $this->setTokenType(null));
			(property_exists($response_obj, 'expires_in')) 
				? $this->setTimeToExpire($response_obj->expires_in + time())
				: $this->setTimeToExpire(null);
			(property_exists($response_obj, 'refresh_token')) 
				? $this->setRefreshToken($response_obj->refresh_token)
				: $this->setRefreshToken(null);
		}

		return $this;
	}

	/**
	 * Exchange authentication information for tokens, depending on grant_type
	 *
	 * @return $this
	 * @throws OAuth2Exception
	 */
	public function authenticate() {
        $config = $this->getConfig();
        $url = $config->getTokenUrl();
		
		$optional_param = array(
			'client_id'		=> $this->getConfig()->getClientId(),
			'client_secret' => $this->getConfig()->getClientSecret()
		);

		$parameters = array(
			'grant_type'	=> $this::PASSWORD_GRANT,
			'username'		=> $this->getUsername(),
			'password'		=> $this->getPassword(),
			'scope'			=> $this->getScope(),
		) + $optional_param;
		
		$addl_options = array(
			CURLOPT_POST			=> true,
			CURLOPT_POSTFIELDS		=> http_build_query($parameters)
		);

		$payload = HTTPRequestService::performRequest($url, $config::IS_SSL_ENABLED, $addl_options);

		if ($this->isValidJson($payload)) {
			$this->handleAccessTokenResponse($payload);
		}

		return $this;
	}

	/**
	 * This flow has specific grant parameters
	 *
	 * @param $scopes
	 * @return array
	 * @throws OAuth2Exception
	 */
	public function getGrantParameters(array $scopes) {
		if (!empty($scopes)) {
			foreach ($scopes as $scope) {
				$this->addScope($scope);
			}
		}

		$optional_params = array(
			'client_secret' => $this->config->getClientSecret(),
			'scope'			=> $this->getScope(),
			'access_type'	=> 'offline'
		);
		$send_tokens = array(
			'client_id'		=> $this->config->getClientId(),
			'username' 		=> $this->username,
			'password' 		=> $this->password,
			'grant_type'	=> $this::PASSWORD_GRANT
		) + $optional_params;

		return $send_tokens;
	}
	
	// Possibly need to implement the BuildOAuthURL method here, but we have no use for it right now.

	/*
	 * Getters and Setters
	 * These are Generic Getters and setters
	 *************************************************/
	/**
	 * @return mixed
	 */
	public function getUsername() {
		return $this->username;
	}

	/**
	 * @param mixed $username
	 *
	 * @return $this
	 */
	public function setUsername($username) {
		$this->username = $username;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getPassword() {
		return $this->password;
	}

	/**
	 * @param mixed $password
	 *
	 * @return $this
	 */
	public function setPassword($password) {
		$this->password = $password;

		return $this;
	}

	/**
	 * @return OAuth2Config
	 */
	public function getConfig() {
		return $this->config;
	}

	/**
	 * @param OAuth2Config $config
	 */
	public function setConfig($config) {
		$this->config = $config;
	}
}