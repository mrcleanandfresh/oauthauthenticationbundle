<?php
/**
 * OAuth2Config.php
 *
 * Author: Kevin Dench <kevin.dench@intellidata.net>
 * Author: IntelliData, Inc.
 * License: Proprietary
 * Version: 1.0
 * Copyright: 2016.
 */

namespace OAuth\AuthenticationBundle\Version\OAuth2;

/**
 * Class OAuth2Config
 * @package Authentication\OAuth\OAuth2
 */
/**
 * Class OAuth2Config
 * @package Authentication\OAuth\OAuth2
 */
class OAuth2Config
{
	/**
	 * Refresh Token Grant
	 * @const REFRESH_TOKEN
	 */
	const REFRESH_TOKEN  = "refresh_token";

	/**
	 * Whether or not we are in a dev environment, or have SSL enabled
	 * @const IS_SSL_ENABLED
	 */
	const IS_SSL_ENABLED = false;

	/**
	 * Whether the user needs to be online or offline for access
	 * @var string $access_type
	 */
	protected $access_type;

	/**
	 * The human readable identifier for the application
	 * @var $application_name
	 */
	protected $application_name;

	/**
	 * The domain where the application is hosted
	 * @var $hosted_domain
	 */
	protected $hosted_domain;

	/**
	 * When the Response from the Authorization Server comes back
	 * It will tell us what we are _actually_ allowed to do
	 * @var array $granted_scopes
	 */
	protected $granted_scopes;

	/**
	 * The response type will always be code
	 * @var string $response_type
	 */
	protected $response_type;

	/**
	 * The client ID that maps the the user of the ID
	 * @var string $client_id
	 */
	private $client_id;

	/**
	 * The client secret associated with the client Id
	 * @var string $client_secret
	 */
	private $client_secret;

	/**
	 * The URL the user wants to redirect to
	 * @var string $redirect_uri
	 */
	private $redirect_uri;

	/**
	 * The static authorization url set via parameters.yml
	 * @var string $authorization_url
	 */
	private $authorization_url;

	/**
	 * The static token url set via parameters.yml
	 * @var string $token_url
	 */
	private $token_url;

	/**
	 * OAuth2Config constructor.
	 *
	 * @param $client_id
	 * @param $client_secret
	 * @param $redirect_uri
	 * @param $response_type
	 * @param $authorization_url
	 * @param $token_url
	 */
	public function __construct($client_id, $client_secret, $redirect_uri, $response_type, $authorization_url, $token_url = null)
	{
		$this->setClientId($client_id);
		$this->setClientSecret($client_secret);
		$this->setRedirectUri($redirect_uri);
		$this->setResponseType($response_type);
		$this->setAuthorizationUrl($authorization_url);
		$this->setTokenUrl($token_url);
	}

	/**
	 * @return string
	 */
	public function getResponseType() {
		return $this->response_type;
	}

	/**
	 * @param string $response_type
	 */
	public function setResponseType($response_type) {
		$this->response_type = $response_type;
	}

	/**
	 * @return string
	 */
	public function getAuthorizationUrl() {
		return $this->authorization_url;
	}

	/**
	 * @param string $authorization_url
	 */
	private function setAuthorizationUrl($authorization_url) {
		$this->authorization_url = $authorization_url;
	}

	/**
	 * @return string
	 */
	public function getTokenUrl() {
		return $this->token_url;
	}

	/**
	 * @param string $token_url
	 */
	private function setTokenUrl($token_url) {
		$this->token_url = $token_url;
	}

	/**
	 * @return mixed
	 */
	public function getAccessType()
	{
		return $this->access_type;
	}

	/**
	 * @param mixed $access_type
	 */
	public function setAccessType($access_type)
	{
		$this->access_type = $access_type;
	}

	/**
	 * @return mixed
	 */
	public function getClientId()
	{
		return $this->client_id;
	}

	/**
	 * @param mixed $client_id
	 */
	private function setClientId($client_id)
	{
		$this->client_id = $client_id;
	}

	/**
	 * @return mixed
	 */
	public function getClientSecret()
	{
		return $this->client_secret;
	}

	/**
	 * @param mixed $client_secret
	 */
	private function setClientSecret($client_secret)
	{
		$this->client_secret = $client_secret;
	}

	/**
	 * @return mixed
	 */
	public function getRedirectUri()
	{
		return $this->redirect_uri;
	}

	/**
	 * @param mixed $redirect_uri
	 */
	public function setRedirectUri($redirect_uri)
	{
		$this->redirect_uri = $redirect_uri;
	}

	/**
	 * @return mixed
	 */
	public function getApplicationName()
	{
		return $this->application_name;
	}

	/**
	 * @param mixed $application_name
	 */
	public function setApplicationName($application_name)
	{
		$this->application_name = $application_name;
	}

	/**
	 * @return mixed
	 */
	public function getHostedDomain()
	{
		return $this->hosted_domain;
	}

	/**
	 * @param mixed $hosted_domain
	 */
	public function setHostedDomain($hosted_domain)
	{
		$this->hosted_domain = $hosted_domain;
	}

	/**
	 * @return mixed
	 */
	public function getGrantedScopes()
	{
		return $this->granted_scopes;
	}

	/**
	 * @param mixed $granted_scopes
	 */
	public function setGrantedScopes($granted_scopes)
	{
		$this->granted_scopes = $granted_scopes;
	}
}