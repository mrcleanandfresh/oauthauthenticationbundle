# OAuthAuthenticationBundle #

This is a Symfony 2 bundle to connect to OAuth1 and OAuth2 servers.

### What is this repository for? ###

* Connecting to OAuth 1 Server
* Connecting to OAuth 2 Server
* Version 1.0

### How do I get set up? ###

* Summary of set up
* [Configuration](#Example Configuration)
* Dependencies
* How to run tests
* Deployment instructions

## Summary of Set up ##

## Example Configuration ##

```
#!yml

o_auth_authentication:
    configuration:
        urls:
            redirect_uri: ~ #OAuthAuthenticationBundle:Controller:OAuthController:OAuthRedirect
            providers:
                twitter: ~
                google_plus: ~
                facebook: ~
                github: 
                    authorization_url: ~
                    token_url: ~
        response_type_default: code
    providers:
        twitter:
            consumer_key: ~
            consumer_secret: ~
            access_token: ~
            access_token_secret: ~
        google_plus:
            client_id: ~
            client_secret: ~
        facebook:
            app_id: ~
            app_secret: ~
        github:
            client_id: ~
            client_secret: ~
```

## Dependencies ##

## Running Tests ##

## Deployment Instructions ##

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact